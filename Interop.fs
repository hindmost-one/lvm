
module Interop

open Types
open Eval

type ('a, 'b) either =
  | Left  of 'a
  | Right of 'b

// A conversion between types 'a and 'b.
//
type ('a, 'b) conversion =
  { forth : 'b -> 'a
    back  : 'a -> 'b
  }

// Composition of 2 conversions.
//
let (<=>)
  (f : ('b, 'c) conversion)
  (g : ('a, 'b) conversion)
     : ('a, 'c) conversion
 =
  { forth = (>>) f.forth g.forth
    back  = (<<) f.back  g.back
  }

let itself : ('a, 'a) conversion =
  { forth = fun a -> a
    back  = fun a -> a
  }

let paired
  ( f : ('a, 'b) conversion
  , g : ('c, 'd) conversion
  )
      : ('a * 'c, 'b * 'd) conversion
 =
  { forth = fun (a, c) -> f.forth a, g.forth c
    back  = fun (b, d) -> f.back  b, g.back  d
  }

let first  f = paired (f, itself)
let second g = paired (itself, g)

// Conversion between 'a and lazy value.
//
type 'a interop = ('a, thunk) conversion

exception ConversionTestFailed of string

// Runs a test by converting `src` back and forth and comparing
// it to the original.
//
let testConv
  (name :   string)
  (conv : ('a, 'b) conversion)
  (src  :  'a)
        :   unit
 =
  if src <> conv.forth (conv.back src)
  then raise <| ConversionTestFailed name
  // else printfn "test passed - %s" name

// Runs a test by converting `src` back and forth and comparing
// it to the original with custom comparator.
//
let testConvComp
  (name :   string)
  (conv : ('a, 'b) conversion)
  (src  :  'a)
  (comp :  'a -> 'a -> bool)
        :   unit
 =
  if not << comp src <| conv.forth (conv.back src)
  then raise <| ConversionTestFailed name
  // else printfn "test passed - %s" name

// Runs a test by converting `src` function back and forth and comparing
// results of application to `x`.
//
let testConvFun
  (name :   string)
  (conv : ('a -> 'c, 'b) conversion)
  (src  :  'a -> 'c)
  (x    :  'a)
        :   unit
 =
  if src x <> conv.forth (conv.back src) x
  then raise <| ConversionTestFailed name
  // else printfn "test passed - %s" name

let bigint : bigint interop =
  { forth = fun value ->
      match value.Force() with
      | VConst (Integer i) -> i
      | thunk -> raise <| Problem "expected int"

    back = fun i -> lazy (VConst <| Integer i)
  }

do testConv "bigint" bigint 123145673290863489561923875612357623I

let int : int interop =
  bigint <=>
  { forth = Operators.int
    back  = fun i -> new System.Numerics.BigInteger (i)
  }

let str : string interop =
  { forth = fun value ->
      match value.Force() with
      | VConst (String i) -> i
      | thunk -> raise <| Problem "expected string"

    back = fun i -> lazy (VConst <| String i)
  }

do testConv "str" str "hello, world"

let float : float interop =
  { forth = fun value ->
      match value.Force() with
      | VConst (Float i) -> i
      | thunk -> raise <| Problem "expected float"

    back = fun i -> lazy (VConst <| Float i)
  }

do testConv "float" float 1.3423432

let value : value interop =
  { forth = fun value -> value.Force()
    back  = fun value -> lazy value
  }

let thunk : thunk interop =
  { forth = fun thunk -> thunk
    back  = fun thunk -> thunk
  }

let forced : thunk interop =
  { forth = fun thunk -> ignore <| thunk.Force(); thunk
    back  = fun thunk -> ignore <| thunk.Force(); thunk
  }

let either
  (left  : 'a interop)
  (right : 'b interop)
         : ('a, 'b) either interop
 =
  { forth = fun value ->
      try
        Left (left.forth value)
      with _ ->
        Right (right.forth value)

    back =
      function
      | Left  l -> left.back  l
      | Right r -> right.back r
  }

let select
  (variants : ('a, 'b) conversion list)
            : ('a, 'b) conversion
 =
  let orElse left right =
    { forth = fun value ->
        try
          left.forth value
        with _ ->
          right.forth value

      back = fun value ->
        try
          left.back value
        with _ ->
          right.back value
    }
  List.reduce orElse variants

do testConv "either/Left"  (either float str) (Left 1.3423432)
do testConv "either/Right" (either float str) (Right "1.3423432")

let (-->)
  (arg :  'a interop)
  (res :  'b interop)
       : ('a -> 'b) interop
 =
  { forth = fun value ->
      res.forth << apply (value.Force()) << arg.back

    back = fun f ->
      lazy (VBIF (res.back << f << arg.forth))
  }

let hasFields
  (fields :  string list)
          : (frame, frame) conversion
 =
  let check frame =
    if Seq.forall (fun f -> Map.containsKey f frame) fields
    then
      frame
    else
      raise <| Problem (sprintf "expected %s" <| string fields)

  { forth = check
    back  = check
  }

let is (thing : 'a) : ('a, 'a) conversion =
  let check thing' =
    if thing = thing'
    then thing'
    else raise <| Problem (sprintf "expected equal to %A" thing)

  { forth = check
    back  = check
  }

do testConvFun "int -> int" (int --> int) (fun x -> x * x) 123

let variant : (string * frame) interop =
  { forth = fun value ->
      match value.Force() with
      | VVariant (ctor, obj, []) -> ctor, obj
      | _ -> raise <| Problem "expected variant"

    back = fun (ctor, obj) ->
      lazy (VVariant (ctor, obj, []))
  }

let object : frame interop =
  { forth = fun value ->
      match value.Force() with
      | VObject      obj
      | VVariant (_, obj, []) -> obj
      | _ -> raise <| Problem "expected object"

    back = fun obj ->
      lazy (VObject obj)
  }

exception No

let constant
  (a : 'a)
  (b : 'b)
     : ('a, 'b) conversion
 =
  { forth = fun b' -> if b = b' then a else raise No
    back  = fun a' -> if a = a' then b else raise No
  }

let bool : bool interop =
  variant <=>
    select
      [ constant true  ("True",  Map.empty)
        constant false ("False", Map.empty)
      ]

do testConv "bool/True"  bool true
do testConv "bool/False" bool false

let list (elem : 'a interop) : 'a list interop =
  let elem' = forced <=> elem
  let rec loop =
    forced <=>
    variant <=>
      select
        [ { forth = fun ("::", map) -> elem'.forth map.["head"] :: loop.forth map.["tail"]
            back  = fun (x :: xs)   -> "::", Map.ofList ["head", elem'.back x; "tail", loop.back xs]
          }

          constant [] ("Nil", Map.empty)
        ]

  loop

do testConv "list int" (list int) [31231; 123; 12; 3112; 3; 1]

let pair
  ( _1 : 'a interop
  , _2 : 'b interop
  )
       : ('a * 'b) interop
 =
  let _1' = forced <=> _1
  let _2' = forced <=> _2
  variant <=>
  paired (is ",", hasFields ["_1"; "_2"]) <=>
  { forth = fun (name, frame) -> (_1'.forth frame.["_1"], _2'.forth frame.["_2"])
    back  = fun (a, c)        -> ",", Map.ofList ["_1", _1'.back a; "_2", _2'.back c]
  }

type fooBar =
  { foo : int
    bar : (string * int) list
    qux : int -> string -> float
  }

let fooBar : fooBar interop =
  let tFoo = int
  let tBar = list <| pair (str, int)
  let tQux = int --> (str --> float)

  object <=>
  hasFields ["foo"; "bar"; "qux"] <=>
  { forth = fun obj ->
      { foo = obj.["foo"] |> tFoo.forth
        bar = obj.["bar"] |> tBar.forth
        qux = obj.["qux"] |> tQux.forth
      }
    back = fun fb ->
      Map.ofList
        [ "foo", fb.foo |> tFoo.back
          "bar", fb.bar |> tBar.back
          "qux", fb.qux |> tQux.back
        ]
  }

do testConvComp "fooBar" fooBar
    { foo = 131
      bar = ["foo", 6; "bar", 7]
      qux = fun i s -> double i + double s.Length
    }
    (fun a b -> a.foo = b.foo
             && a.bar = b.bar
             && a.qux 7 "hello" = b.qux 7 "hello")
