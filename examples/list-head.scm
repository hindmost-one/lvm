(let
  ((push  (ctor Push head tail))
   (empty (ctor Empty))
   (list  (push 1 (push 2 (push 3 empty))))
  )
(? list Push (head tail) (+ 1 head) 0))