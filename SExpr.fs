
module SExpr

(*
  A parser for s-expr.
*)

type sexpr =
  | Name of string
  | Str  of string
  | Int  of bigint
  | List of sexpr list

type substr = string * int

// A type for parser. Useful to find errors.
//
type 'a parser = substr -> 'a * substr

exception Unexpected of int

// I snatched this from http://www.fssnip.net/5u/title/String-explode-and-implode
//
module String =
  /// Converts a string into a list of characters.
  let explode (s:string) =
    [for c in s -> c]

  /// Converts a list of characters into a string.
  let implode (xs:char list) =
    let sb = System.Text.StringBuilder(xs.Length)
    xs |> List.iter (sb.Append >> ignore)
    sb.ToString()

// Basic parser. Returns a char, if `pred` is fine with it.
//
let satisfying (pred : char -> bool) : char parser =
  fun (text : string, pos) ->
    if pred text.[pos]
    then
      text.[pos], (text, pos + 1)
    else
      raise <| Unexpected pos

// Predicate, checks if char is in string.
//
let elem (s : string) (c : char) : bool = String.exists ((=) c) s

// Usable parsers
//
let oneOf  (s : string) : char parser = satisfying (elem s)
let noneOf (s : string) : char parser = satisfying (not << elem s)

// Parse this char only.
//
let char (c : char) : char parser = satisfying ((=) c)

let space : char parser = oneOf " \n\r"

// Just return a value, don't do anything.
//
let just (x : 'a) : 'a parser = fun s -> x, s

// If left parser fails, try right one.
//
let orElse (l : 'a parser) (r : 'a parser) : 'a parser =
  fun s ->
    try
      l s
    with _ ->
      r s

// Try parsers one-by-one until any succeeds.
//
let rec choose (ps : 'a parser list) : 'a parser = List.reduce orElse ps

// One or more repeats of p.
//
let rec some (p : 'a parser) : 'a list parser =
  fun s ->
    let x,  s = p s
    let xs, s = many p s
    x :: xs, s

// Zero or more repeats of p.
//
and many (p : 'a parser) : 'a list parser =
  orElse (some p) (just [])

// Apply a function to the result of parser.
//
let map (f : 'a -> 'b) (p : 'a parser) : 'b parser =
  fun s ->
    let a, s = p s
    f a, s

// Ignore the result of parser.
//
let voided (p : 'a parser) : unit parser = map (fun _ -> ()) p

let spaces : unit parser = voided (many space)

// Repeats of p with sep in between.
//
let rec someSepBy (sep : 'sep parser) (p : 'a parser) : 'a list parser =
  fun s ->
    let x,  s = p s
    let _,  s = sep s
    let xs, s = manySepBy sep p s
    x :: xs, s

and manySepBy (sep : 'sep parser) (p : 'a parser) : 'a list parser =
  orElse (someSepBy sep p) (just [])

// Ignore result of parser; return part of the text it crawled through instead.
//
let trace (p : 'a parser) : string parser =
  fun (t : string, i : int) ->
    let _, (_, j) = p (t, i)
    t.Substring(i, j - i), (t, j)

// Attach parser-producing callback to the parser.
//
let bind (p : 'a parser) (k : 'a -> 'b parser) : 'b parser =
  fun s ->
    let a, s = p s
    k a s

// Run a, then b. Return result of a.
//
let and_ (a : 'a parser) (b : 'b parser) : 'a parser =
  bind a (fun x ->
  bind b (fun _ ->
    just x))

// Run a, then b. Return result of b.
//
let _and (a : 'a parser) (b : 'b parser) : 'b parser = bind a (fun _ -> b)

// Run parsers in sequence, ignore their result.
//
let sequence_ (ps : unit parser list) : unit parser = List.fold _and (just ()) ps

// Parse a '-delimited string.
//
let str =
  map Str
    (_and (char '\'')
    (and_ (trace (many (noneOf "'")))
          (char '\'')))

let digit : char parser = satisfying (elem "0123456789")

// Parse a positive integer.
//
let int  = map (Int << bigint.Parse) (trace (some digit))

// Parse a name.
//
let name = map Name (trace (some (satisfying (not << elem "(#') \t\n\r"))))

// Parse spaces before and after p.
//
let t (p : 'a parser) : 'a parser = _and spaces (and_ p spaces)

// Delay parser construction (for recursive parsers).
//
let delayed (thunk : 'a parser Lazy) : 'a parser = fun s -> (thunk.Force()) s

// Parse ( and ) around p.
//
let parens (p : 'a parser) : 'a parser =
  (_and (t (char '('))
  (and_  p
        (t (char ')'))))

// Parse SExpr. Will parse "() () ()" as one list with 3 elems.
//
let rec list : sexpr parser =
  map List (manySepBy spaces value)

and value : sexpr parser =
  choose
    [ str
      int
      name
      parens (delayed (lazy list))
    ]

// Parse from file.
//
let fromFile (p : 'a parser) (name : string) : 'a =
  let text = System.IO.File.ReadAllText(name)
  fst (p (text, 0))
