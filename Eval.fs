
module Eval

open Types
open PrettyPrint

exception Problem of description : string
exception Crash

// The evaluator. Implementation is an adaptation of:
//   https://github.com/sciolizer/lazy-interpreter/blob/master/Interpreter.hs
//
let rec eval
  (stack   : stack)
  (program : program)
           : thunk
 =
  stackTraced stack program <| fun () ->
  match program with
  | Var name ->
    lookup stack name

  // We turn decls into a stack frame,
  // and eval body with that additional frame.
  //
  | Let (scope, body) ->
    let frame = evalScope stack scope
    eval (frame :: stack) body

  // We force the evaluation of function,
  // but the argument is still lazy.
  //
  | Call (f, x) ->
    let f = eval stack f |> force
    let x = eval stack x
    apply f x

  // Creation of "untyped" object.
  //
  | Create names ->
    let values = List.map (lookup stack) names
    let names  = List.map raw names

    lazy (Seq.zip names values |> Map.ofSeq |> VObject)

  // Inspection of values. If it has "type" ctor and "fields",
  // we run "yes"-branch; otherwise we run "no"-branch.
  //
  | IfMatch (subject, ctor, fields, yes, no) ->
    let subj = eval stack subject |> force
    match subj with
    | VError msg -> lazy (VError msg)
    | _ ->
      match deconstruct ctor fields subj with
      | Some frame -> eval (frame :: stack) yes
      | None       -> eval           stack  no

  // The lambda captures current stack, so the scope is static.
  //
  | Lambda (arg, body) ->
    lazy (VClosure (stack, arg.raw, body))

  // If there are fields, it's a function from these fields.
  // Otherwise, it's an object with fields an a "type".
  //
  | Ctor (name, fields) ->
    lazy (VVariant (name.raw, Map.empty, List.map raw fields))

  | Const c ->
    lazy (VConst c)

  | Invoke (name, program) ->
    eval (Invoked name :: stack) program

// Search the stack for a value.
//
and lookup
  (stack : stack)
  (name  : name)
         : thunk
 =
  let rec loop =
    function
    | LetFrame frame :: _
      when Map.containsKey name.raw frame ->
        frame.[name.raw]

    | ArgFrame (name', value) :: _
      when name' = name.raw ->
        value

    | _ :: rest ->
      loop rest

    | [] ->
      raise <| Problem (sprintf "Undefined %s" name.raw)
  loop stack

// Inspect a value.
//
and deconstruct
  (ctor   : name)
  (fields : name list)
  (value  : value)
          : stack_frame option
 =
  let fields = List.map raw fields
  match ctor.raw, value with

  // Untyped object?
  //
  | "__Object", VObject frame ->
    if Seq.forall (fun field -> Map.containsKey field frame) fields
    then
      Map.filter (fun k _ -> Seq.contains k fields) frame
      |> LetFrame
      |> Some
    else
      None

  // Constant?
  //
  | "__Integer",   VConst (Integer _)
  | "__String",   VConst (String  _)
  | "__Float", VConst (Float   _) ->
    Map.empty
    |> LetFrame
    |> Some

  // Typed object of correct "type"?
  //
  | _, VVariant (ctor', bound, []) when ctor' = ctor.raw ->
    Map.filter (fun k _ -> Seq.contains k fields) bound
    |> LetFrame
    |> Some

  // Typed object of incorrect "type"?
  //
  | _, VVariant (ctor', bound, []) ->
    None

  // A function?
  //
  | _, VBIF _
  | _, VClosure _
  | _, VVariant _ ->
    raise <| Problem (sprintf "Cannot deconstruct %O" value)

  // Whatever else?
  //
  | _ ->
    None

// We have a bunch of declarations.
// We heed to make a stack frame of them.
// The problem is, each declaration of the frame
// should be evaluated in the very frame
// we must return.
//
// Therefore there's all that `rec`/`lazy`/`force` trickery.
// Oh, and we convert a `name`-keyed `Map` into the `string`-keyed one.
//
and evalScope
  (stack : stack)
  (scope : (name, program) Map)
         : stack_frame
 =
  let rec frame =
    scope
    |> Map.toSeq
    |> Seq.map (fun (k, v) -> k.raw, v)
    |> Map.ofSeq
    |> Map.map (fun _ v -> lazy (force <| eval (frame :: stack) v))
    |> LetFrame

  frame

// Apply a function to an argument.
//
and apply (f : value) : thunk -> thunk =
  match f with

  // A normal function?
  // Restore its stack, push `x` into it, eval body.
  //
  | VClosure (stack, arg, body) ->
    fun x ->
      let stack' = ArgFrame (arg, x) :: stack
      eval stack' body

  // A variant constructor with some unbound fields?
  // Add `x` as the top field.
  //
  | VVariant (ctor, bound, top :: free) ->
    fun x ->
      lazy (VVariant (ctor, Map.add top x bound, free))

  // A builtin?
  // Just run it.
  //
  | VBIF bif ->
      bif

  // An error?
  // Return the same error.
  //
  | VError msg ->
    fun x ->
      lazy (VError msg)

  // Something else?
  //
  | f ->
    raise <| Problem (sprintf "Not a function %O" f)

// Forcefully evaluate lazy value.
//
and force (x : thunk) : value = x.Force()

and stackTraced (stack : stack) (program : program) k =
  try
    k ()
  with
    | Crash ->
      raise Crash

    | Problem msg ->
      printfn "An exception '%s'" msg
      printfn "while evaluating"
      ppProgram 2 program
      printfn ""
      ppStack stack
      raise Crash
