
# Wat

The project is an interpreter of a simple LISP-like (or, rather _scheme-like_) language.

# Usage

```sh
$ dotnet run -- fib.scr
fib 12 = VConst (Integer 233)
VConst (Integer 54289)
```

# Language

## Constants

Only non-negative integers (of any size, `bigint`) and `'`-delimited strings without escapes and `'` inside are supported.

## Builtins

There are following builtin functions: (over ints) `+`, `*`, `-`, `=`, (debug print) `trace`, (terminate) `error`

Use them like this:

```lisp
(= x 2)
(+ 1 2)
```

## Lambdas

```lisp
(-> x (+ x 1))
```

## Let-expressions

The _values_ are _mutually-recursive_.

```lisp
(let
  ((x  1)
   (y (+ x 1))
  )
(+ x y))
```

By the way! You can do this:

```lisp
(let
  ((inc (+ 1))
   (x    5)
  )
(inc x))
```

The result is `6`.

```lisp
(let
  ((flip (-> f (-> x (-> y (f y z)))))
   (-?   (flip -))
  )
(-? 5 1))
```

The result is `-4`.

## Branching

Looks scary.

```lisp
(? (= x 0) True () 'Is zero' 'not a zero')
```

```lisp
(let
  ((push  (ctor Push head tail))
   (empty (ctor Empty))
   (list  (push 1 (push 2 (push 3 empty))))
  )
(? list Push (head tail) (+ 1 head) 0))
```

The result is `2`.

## Objects

```lisp
(let
  ((sqr (-> x (* x x)))
   (x2  (-> x (+ x x)))
   (obj (new sqr x2))
  )
obj)
```