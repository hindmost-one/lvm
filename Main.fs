
open Types
open PrettyPrint
open Eval
open SExpr
open Parser
open Stdlib

match System.Environment.GetCommandLineArgs() with
| [|_; filename|] ->
  try
    fromFile list filename    // read the sexpr
    |> fromSExpr              // convert to program
    |> eval [LetFrame stdlib] // evaluate to lazy value
    |> pp 0                   // print

  with
    | e -> printfn "Program crashed (%O)" e

| arr ->
  printfn "USAGE: dotnet run -- script-name, not %A" arr