
module Parser

open Types
open SExpr

exception CannotParse of sexpr

let rec fromSExpr (sexpr : sexpr) : program =
  match sexpr with
  | Name a -> Var (n a)
  | Int  i -> Const (Integer i)
  | Str  s -> Const (String  s)

  | List [Name "let"; List decls; body] ->
    let decls = List.map declFromSExpr decls
    let body  = fromSExpr body
    Let (Map.ofList decls, body)

  | List [Name "?"; subj; Name ctor; List fields; yes; no] ->
    let subj   = fromSExpr subj
    let  ctor   = n ctor
    let fields = List.map fieldFromSExpr fields
    let yes    = fromSExpr yes
    let no     = fromSExpr no
    IfMatch(subj, ctor, fields, yes, no)

  | List (Name "new" :: fields) ->
    let fields = List.map fieldFromSExpr fields
    Create fields

  | List (Name "ctor" :: Name name :: fields) ->
    let name = n name
    let fields = List.map fieldFromSExpr fields
    Ctor (name, fields)

  | List [Name "->"; Name arg; body] ->
    let arg = n arg
    let body = fromSExpr body
    Lambda (arg, body)

  | List chain ->
    let chain = List.map fromSExpr chain
    List.reduce (fun f x -> Call (f, x)) chain

and declFromSExpr (decl : sexpr) : name * program =
  match decl with
  | List [Name name; expr] -> (n name, Invoke (n name, fromSExpr expr))
  | _                      -> raise <| CannotParse decl

and fieldFromSExpr (field : sexpr) : name =
  match field with
  | Name name -> n name
  | _         -> raise <| CannotParse field
