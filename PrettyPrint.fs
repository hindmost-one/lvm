
module PrettyPrint

open Types

exception CantPPPS

let rec tab (indent : int) : string =
  match indent with
  | 0 -> ""
  | n -> "  " + tab (indent - 1)

let rec pp (indent : int) (thunk : thunk) : unit =
  match thunk.Force() with
  | VConst  c -> ppConst indent c

  | VObject frame ->
    printfn "%s{" (tab indent)

    for (name, field) in Map.toSeq (padFrame frame) do
      if oneLine field
      then
        printf "%s%s = " (tab (indent + 1)) name
        pp 0 field
      else
        printfn "%s%s =" (tab (indent + 1)) name
        pp (indent + 2) field

    printfn "%s}" (tab indent)

  | VClosure (_, arg, body) ->
    if oneLiner body
    then
      printfn "%s%s -> %s" (tab indent) arg (ppps body)

    else
      printfn "%s%s ->" (tab indent) arg
      ppProgram (indent + 1) body

  | VBIF _ ->
    printfn "%s<builtin>" (tab indent)

  | VVariant (ctor, frame, []) ->
    printfn "%s(%s) {" (tab indent) ctor

    for (name, field) in Map.toSeq (padFrame frame) do
      if oneLine field
      then
        printf "%s%s = " (tab (indent + 1)) name
        pp 0 field
      else
        printfn "%s%s =" (tab (indent + 1)) name
        pp (indent + 2) field

    printfn "%s}" (tab indent)

  | VVariant (ctor, frame, free) ->
    let prefix = List.map (sprintf "-> %s") free |> List.reduce (+)
    printfn "%s%s" (tab indent) prefix
    pp (indent + 1) (lazy (VVariant (ctor, frame, [])))

  | VError msg ->
    printfn "%serror %s" (tab indent) msg

and ppConst (indent : int) (c : constant) : unit =
  match c with
  | Integer i -> printfn "%s%A" (tab indent) i
  | Float   i -> printfn "%s%A" (tab indent) i
  | String  i -> printfn "%s%A" (tab indent) i

and ppProgram (indent : int) (program : program) : unit =
  if oneLiner program
  then
    printfn "%s%s" (tab indent) (ppps program)
  else
    match program with
    | Var n ->
      printfn "%s%s" (tab indent) n.raw

    | Let (ds, b) ->
      printfn "%slet" (tab indent)

      for (name, field) in Map.toSeq (padCtx ds) do
        if oneLiner field
        then
          printfn "%s%s = %s" (tab (indent + 1)) name.raw (ppps field)

        else
          printfn "%s%s =" (tab (indent + 1)) name.raw
          ppProgram (indent + 2) field

      printfn ""
      ppProgram indent b

    | Call (f, x) ->
      printfn "%s(" (tab indent)
      ppProgram (indent + 1) f
      ppProgram (indent + 1) x
      printfn "%s)" (tab indent)

    | IfMatch (s, c, fs, y, n) ->
      if oneLiner s
      then
        printfn "%sif %s is %s {%s}" (tab indent) (ppps s) c.raw (unwords (List.map raw fs))
        printfn "%sthen" (tab indent)
        ppProgram (indent + 1) y
        printfn "%selse" (tab indent)
        ppProgram (indent + 1) n

      else
        printfn "%sif" (tab indent)
        ppProgram (indent + 1) s
        printfn "%sis %s {%s}" (tab indent) c.raw (unwords (List.map raw fs))
        printfn "%sthen" (tab indent)
        ppProgram (indent + 1) y
        printfn "%selse" (tab indent)
        ppProgram (indent + 1) n

    | Create ns ->
      printfn "%s%s" (tab indent) (ppps program)

    | Ctor (c, ns) ->
      printfn "%s%s{%s}" (tab indent) c.raw (unwords (List.map raw ns))

    | Lambda (a, b) ->
      printfn "%s%s ->" (tab indent) a.raw
      ppProgram (indent + 1) b

    | Const c -> ppConst indent c

    | Invoke (_, p) -> ppProgram indent p

and ppps (program : program) : string =
  match program with
  | Var n -> n.raw
  | Const c -> pppc c

  | Create ns ->
    sprintf "{%s}" (unwords (List.map raw ns))

  | Ctor (c, ns) ->
    sprintf "%s{%s}" c.raw (unwords (List.map raw ns))

  | Call (f, x) ->
    "(" + ppps f + " " + ppps x + ")"

  | Lambda (a, b) ->
    sprintf "%s -> %s" a.raw (ppps b)

  | _ ->
    raise CantPPPS

and pppc (c : constant) : string =
  match c with
  | Integer i -> sprintf "%A" i
  | Float   i -> sprintf "%A" i
  | String  i -> sprintf "%A" i

and oneLine (thunk : thunk) : bool =
  match thunk.Force() with
  | VConst   _
  | VBIF     _
  | VError   _
    -> true

  | VClosure (_, _, b) ->
    oneLiner b

  | _
    -> false

and oneLiner (program : program) : bool =
  match program with
  | Var   _
  | Const _
  | Create _
    -> true

  | Call   (f, x) -> oneLiner f && oneLiner x
  | Lambda (_, b) -> oneLiner b

  | _
    -> false

and padNamesRight
  (forth : 'a -> string)
  (back  : string -> 'a -> 'a)
  (frame : ('a, 'b) Map)
         : ('a, 'b) Map
 =
  let list = Map.toList frame
  let len =
    list
    |> List.map fst
    |> List.map (fun a -> (forth a).Length)
    |> (fun l -> 0 :: l)
    |> List.max

  let rec pad n s =
    match n with
    | 0 -> s
    | n -> pad (n - 1) (s + " ")

  let mapped = List.map (fun (n, f) -> back (pad (len - (forth n).Length) (forth n)) n, f) list
  Map.ofList mapped

and padFrame f = padNamesRight (fun s -> s) (fun s _ -> s) f
and padCtx f =
  padNamesRight (fun s -> s.raw) (fun s n -> { n with raw = s}) f

and unwords =
  function
  | []   -> ""
  | list -> List.reduce (fun a b -> a + " " + b) list

let rec ppStack =
  List.iter ppStackFrame

and ppStackFrame =
  function
  | LetFrame frame ->
    printfn "Local declarations:"
    for (n, v) in Map.toList (padFrame frame) do
      printDecl n v
    printfn ""

  | ArgFrame (n, v) ->
    printfn "Argument:"
    printDecl n v
    printfn ""

  | Invoked n ->
    printfn "Calling: %s" n.raw
    printfn ""

and printDecl n v =
  let isCreated = v.IsValueCreated
  if not isCreated
  then
    printfn "  %s = <?>" n
  else
    if oneLine v
    then
      printf "  %s = " n
      pp 0 v
    else
      printfn "  %s = <too long>" n
